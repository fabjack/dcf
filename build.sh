#!/bin/bash

# this is a mostly useless builder script
# to run DCF, launch run.py
#
# useful only if you need an executable file

rm -rf dist build
pyinstaller run.py -n dcf --onefile

