#!/usr/bin/env python3
# coding: utf8

import sys
import tempfile
import logging
import json
import time
from platform import system

from retrying import retry
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

import scrapper
import settings


class MappyScrapper():

    def __init__(self, url):
        self.mails = []
        self.url_site = url
        self.kw = None

    def export(self, content, csv_sep):
        temp_dir = tempfile.gettempdir()
        dir_sep = '\\' if system() == 'Windows' else '/'
        export_file = "{}{}mappy.{}.csv".format(temp_dir, dir_sep, self.kw.replace(' ', '-'))
        logging.debug('export file : %s', export_file)
        with open(export_file, 'w') as f:
            nb_lines = 0
            x = content[0]
            for col in x:
                f.write(col)
                f.write(csv_sep)
            f.write('\n')
            for x in content:
                for col in x.values():
                    if type(col) == list:
                        f.write(csv_sep.join(col))
                    else:
                        f.write(col)
                    f.write(csv_sep)
                f.write('\n')
                nb_lines += 1

    @retry(wait_random_min=3000, wait_random_max=9000, stop_max_attempt_number=5)
    def surf_to_site(self, driver, kw):
        logging.info('Surfing to %s ...', self.url_site)
        driver.get(self.url_site)
        logging.info(driver.title)
        time.sleep(1)
        driver.find_element_by_name("search").send_keys(kw)
        driver.find_element_by_name("search").send_keys(Keys.ENTER)

    @retry(wait_random_min=3000, wait_random_max=9000, stop_max_attempt_number=5)
    def find_prospect_elements(self, driver):
        res = []
        elements = driver.find_elements_by_class_name("marker-poi")
        for elt in elements:
            res.append(elt)
        return res

    def scrap(self, kw):
        self.kw = kw
        options = Options()
        options.add_argument("--headless")

        driver = webdriver.Firefox(firefox_options=options)
        driver.implicitly_wait(1)
        self.surf_to_site(driver, kw)
        time.sleep(7)
        res = self.find_prospect_elements(driver)

        for elt in res:
            logging.info(elt._id)
            try:
                elt.click()
            except:
                break
            time.sleep(2)

            # find name + address
            div_elts = driver.find_elements_by_class_name('geoentityPopinView-poiMetadata')
            if div_elts:
                contact = div_elts[0].text.replace('\n', settings.csv_sep)

                # find email
                email = ''
                for link in driver.find_elements_by_tag_name('a'):
                    try:
                        href = link.get_attribute('href')
                        if href.startswith('mailto:'):
                            email = href[7:]
                    except:
                        pass

                # store data
                dict = {'mail': email, 'contact': contact}
                logging.info(dict)
                self.mails.append(dict)

        driver.quit()

if __name__ == '__main__':

    _loglevel = logging.INFO
    _logfilename = 'dcf.log'
    _key_words = None
    argc = len(sys.argv)
    i = 1
    while (i < argc):
        if sys.argv[i] in ('--log', '--level', '--loglevel', '--log-level'):
            _loglevel = getattr(logging, sys.argv[i+1].upper())
            i += 2
        elif sys.argv[i] in ('--console', '--tty', '--dev', '--test'):
            _logfilename = ''
            i += 1
        elif sys.argv[i] in ('--kw', '--key-words'):
            _key_words = sys.argv[i+1]
            i += 2
        else:
            print ("unknown argument : %s" % (sys.argv[i]))
            sys.exit(1)

    if _key_words == None:
        print('Missing KW')
        sys.exit(1)

    if _logfilename != '':
        temp_dir = tempfile.gettempdir()
        dir_sep = '\\' if system() == 'Windows' else '/'
        _logfilename = temp_dir + dir_sep + _logfilename

    logging.basicConfig(format='%(asctime)s [%(filename)s] [%(funcName)s:%(lineno)d] [%(levelname)s] %(message)s', filename=_logfilename, level=_loglevel, filemode='w')
    logging.info('Starting DCF version {}.{}.{}'.format(
        settings.software['version_major'],
        settings.software['version_minor'],
        settings.software['version_patch']))
    logging.debug('Settings : {} {}'.format(settings.nb_pages_to_scrap, settings.nb_serp_results))
    s = MappyScrapper('http://mappy.com')
    s.scrap(_key_words)
    if s.mails:
        logging.info("{} mail(s) found".format(len(s.mails)))
        s.export(s.mails, settings.csv_sep)
    else:
        logging.info('No mail found')
