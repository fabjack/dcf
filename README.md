# DCF - Domain Client Finder

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://bitbucket.org/fabjack/DCF/src/master/LICENSE.txt)

## About

[Domain Client Finder](http://domainologic.com/dcf/) helps domainers in their outbound sales process.
It scraps the web to find emails of propects.

## Quickstart

The software requires python 3.6 or higher.
To download the source code, you'll need a git client.
It has been tested on:
 - Linux
 - Windows 7
 - MacOS

To install, setup and run the software:

    $ git clone https://fabjack@bitbucket.org/fabjack/dcf.git
    $ cd dcf
    $ ./setup.py
    $ ./run.py
