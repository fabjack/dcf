# coding: utf8

import os
import logging
import tempfile

def html_decode(h):
    start = 0
    pos = h.find('%')
    while pos >= 0:
        ori_hexa_code = h[pos:pos+3]
        hexa_code = ori_hexa_code.replace('%','0x').lower()
        char = chr(int(hexa_code,16))
        h = h.replace(ori_hexa_code, char)
        pos = h.find('%')
    return h

def get_tld_cache_file():
    temp_dir = tempfile.gettempdir()
    cache_file = temp_dir + os.path.sep + 'dcf_tld_cache.txt'
    logging.debug('TLD cache file : {}'.format(cache_file))
    return cache_file

