# coding: utf8

software = {"version_major": 1,
        "version_minor": 1,
        "version_patch": 0,
        "name": 'Domain Client Finder',
        "editor": 'Domainologic'
        }

nb_pages_to_scrap = 10
nb_serp_results = 1000
csv_sep = ','

headers = {'user-agent': 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)',
    'Accept-Language': 'en-US,en;q=0.5'}

