# coding: utf8
 
import time
import requests
from unidecode import unidecode
from lxml import html
from tkinter import *
import tldextract
import regex
import random
import logging

from utils import get_tld_cache_file, html_decode
import settings

mail_re = [
        r'.*[" >:]([a-zA-Z0-9-\.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]+)[" <].*',
        r'.*[" >:]([a-zA-Z0-9-\.]+ @ [a-zA-Z0-9-]+\.[a-zA-Z0-9]+)[" <].*',
        r'.*[" >:]([a-zA-Z0-9-\.]+ @ [a-zA-Z0-9-]+ \. [a-zA-Z0-9]+)[" <].*',
        r'.*[" >:]([a-zA-Z0-9-\.]+ [aA][tT] [a-zA-Z0-9-]+\.[a-zA-Z0-9]+)[" <].*',
        r'.*[" >:]([a-zA-Z0-9-\.]+ [aA][tT] [a-zA-Z0-9-]+ \. [a-zA-Z0-9]+)[" <].*',
        r'.*[" >:]([a-zA-Z0-9-\.]+ [aA][tT] [a-zA-Z0-9-]+ [dD][oO][tT] [a-zA-Z0-9]+)[" <].*'
        ]

mail_kw = ['contact', 'mail', 'team', 'info']

class Scrapper():

    def __init__(self, url, gui, nb_pages):
        self.scrapped = []
        self.mails = []
        self.tree = None
        self.gui = gui
        self.url_site = url
        self.nb_pages = nb_pages
        self.te = tldextract.TLDExtract(cache_file=get_tld_cache_file())
        self.dn = 'http://' + self.te(self.url_site).registered_domain
        self.r = None

    def isSameDomain(self, d):
        d1 = self.te(self.url_site)
        d2 = self.te(d)
        return d1.domain == d2.domain and d1.suffix == d2.suffix

    def absoluteDomain(self, url):
        if len(url) == 0:
            url = self.dn + '/'
        elif len(url) < 4 or url[:4] != 'http':
            if url[0] == '/':
                url = self.dn + url
            else:
                url = self.dn + '/' + url
        return url

    def extract_mail(self, m):
        logging.info('extracted mail #1 : %s' % m)
        m = m.strip()
        pos = m.find(':')
        m = m[pos+1:]
        pos = m.find('?')
        if pos >= 0:
            m = m[:pos]
        logging.info('extracted mail #2 : %s' % m)
        if m != '':
            self.mails.append(m)

    def search_mails(self):
        for mre in mail_re:
            found_mails = regex.findall(mre, self.r.text)
            for m in found_mails:
                self.extract_mail(m)
            mre = mre.replace('"', "'")
            found_mails = regex.findall(mre, self.r.text)
            for m in found_mails:
                self.extract_mail(m)

    def scrap(self, url=''):
        self.gui.update()
        if self.gui.cancel_requested:
            logging.debug('cancel requested')
            return
        if self.absoluteDomain(url) in self.scrapped:
            return
        if 'mailto:' in url:
            self.extract_mail(url)
            self.scrapped.append(self.absoluteDomain(url))
            return
        if ':' in url:  # tel:
            self.scrapped.append(self.absoluteDomain(url))
            return
        if url.endswith('.pdf'):
            return
        if '#' in url:
            return
        if len(self.scrapped) >= self.nb_pages:
            return

        self.scrapped.append(self.absoluteDomain(url))
        if url == '':
            url = self.url_site
        else:
            url = self.absoluteDomain(url)
        if '#' in url:
            pos = url.find('#')
            url = url[:pos]
        if '%' in url:
            url = html_decode(url)

        if self.isSameDomain(url):
            logging.info('scrapping : %s' % url)
            try:
                self.r = requests.get(url, headers=settings.headers, timeout=10)
                logging.debug("status code : %d" % self.r.status_code)
                if 200 <= self.r.status_code < 300:
                    self.tree = html.fromstring(self.r.content)
                    self.search_mails()
                    self.scrap_next()
                time.sleep(random.randint(1,2))
            except requests.TooManyRedirects:
                logging.warning('Too many redirects')
            except Exception as e:
                try:
                    logging.warning(e.message)
                except:
                    logging.warning('unknown exception')

    def scrap_next(self):
        links = self.tree.xpath('//a')
        for link in links:
            if self.gui.cancel_requested:
                break
            if link.text is not None and any(kw in link.text for kw in mail_kw):
                self.scrap(link.text)
        if len(self.mails) == 0:
            links = self.tree.xpath('//a/@href')
            for link in links:
                if self.gui.cancel_requested:
                    break
                self.scrap(link)

